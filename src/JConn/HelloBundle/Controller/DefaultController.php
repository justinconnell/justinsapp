<?php

namespace JConn\HelloBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('JConnHelloBundle:Default:index.html.twig');
    }
}
